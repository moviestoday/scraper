var csoon = require("./cinemax_getComingSoon"),
    nshow = require("./cinemax_getNowShowing"),
    cinemax = module.exports = {};

cinemax.getNowShowing = nshow.getNowShowing;
cinemax.getComingSoon = csoon.getComingSoon;
