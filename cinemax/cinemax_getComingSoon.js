var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    cheerio = require("../utils/utils.js").cheerio,
    stringUtils = require("../utils/utils.js").stringUtils,
    workPool = require("../utils/utils.js").async,
    core = require("../core/core"),
    rootUrl = "http://centurycinemax.co.ug/?cat=3",
    cinemax = module.exports = {},

    loadStartPage = function(opts){
        return loader.loadDoc("rootUrl",opts)
    },

    getMovies = function(opts){
        return new Promise(function(gm_resolve,gm_reject){
            $ = cheerio.load(opts["body"]);

            var querySelector = ".post_title > h3 > a",
                rawMovies = $(querySelector),
                movies = [],
                gm_Async = [];

            workPool.each(rawMovies,function(rawMovie,callback){
                var name = $(rawMovie).text();
                var _3D = stringUtils(name).contains("3D");
                name = name.replace("3D","").replace("2D","").trim()
                var lastChar = name.charAt(name.length-1)+"";
                if(!stringUtils(lastChar).isAlpha() ){
                   name = name.substring(0,name.length-1).trim()
                }
                movies.push({"title":name,"cinema":"cinemax","_3D":_3D,"poster":"http://teradactol.github.io/moviestoday/poster_404.png"});
                callback();
            },function(err){
                opts["movies"] = movies;
                gm_resolve(opts);
            })
        });
    },

    getShowTimes = function(opts){
        return new Promise(function(gst_resolve,gst_reject){
            $ = cheerio.load(opts["body"]);
            var querySelector = "#text-2 > div > p",
                rawShowTimes = $(querySelector).html();//.replaceAll('(.*)','').split('<br>\n')
            rawShowTimes = rawShowTimes+"";
            rawShowTimes = rawShowTimes.replace(/\(\w*\):/g,'').split('<br>\n');
            opts["movies"].forEach(function(movie,idx){
                var title = movie.title.toLowerCase();
                for(var i =0; i < rawShowTimes.length; i++){
                    var timestr = rawShowTimes[i].toLowerCase();
                    if(timestr.search(title)>-1){
                        var tokens = timestr.split(title), timestr = tokens[tokens.length-1];
                        opts["movies"][idx]["showtimes"]=timestr.replace('.','').replace(/,.*/g,'').split('/');
                        break;
                    }
                }
            });
            gst_resolve(opts);
        });
    }
    ;

cinemax.getComingSoon = function(){
  return new Promise(function(gcs_resolve,gcs_reject){

    var opts = {};
    opts["rootUrl"] = rootUrl;
    loadStartPage(opts)
    .then(getMovies)
    .then(getShowTimes)
    .then(core.scrape)
    .then(function(movies){
        gcs_resolve(movies);
    });

  });
}
