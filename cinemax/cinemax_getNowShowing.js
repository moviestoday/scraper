var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    request = require('request'),
    cinemax_api = 'https://sheetsu.com/apis/7d6e1126',
    workPool = require("../utils/utils.js").async,
    core = require("../core/core"),
    rootUrl = "http://centurycinemax.co.ug/?cat=5",
    cinemax = module.exports = {},
//========= Because of sheetsu's shitty code, had to copy this and hack it to fix it quick=============
    getAllData = function(callback) {
        var get_url = cinemax_api;
        var request_params = {
            url : get_url,
            method: 'GET'
        };
        request(request_params, function(err, response, body) {
            if(err) {
                throw(err);
            }
            body = JSON.parse(body);
            if(body.status === 200 && body.success === true) {
                var results = body.result;
                callback({meta:{code: 200}, data: results});
            }
        });
    },
//=============================================================================================================
    loadStartPage = function(opts){
        return loader.loadDoc("rootUrl",opts)
    },

    getMovies = function(opts){
        return new Promise(function(gm_resolve,gm_reject){
            getAllData(function(response){
                var rawMovies = response.data, movies=[];
                workPool.each(rawMovies,function(rawMovie,callback){
                    var name = rawMovie.title;
                    var _3D = rawMovie._3d;
                    var showtimes = rawMovie.showtimes.split("\n");
                    movies.push({"title":name,"cinema":"cinemax","_3D":_3D,"poster":"http://teradactol.github.io/moviestoday/poster_404.png","showtimes":showtimes});
                    callback();
                },function(err){
                    opts["movies"] = movies;
                    gm_resolve(opts);
                })
            });
        });
    }
;

cinemax.getNowShowing = function(){
  return new Promise(function(gns_resolve,gns_reject){
    var opts = {};
    opts["rootUrl"] = rootUrl;
    loadStartPage(opts)
    .then(getMovies)
    .then(core.scrape)
    .then(function(opts){
        gns_resolve(opts['n_movies']);
    })
  });
};
