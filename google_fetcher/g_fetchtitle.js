var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    stringUtils = require("../utils/utils.js").stringUtils,
    b64 = require("../utils/utils.js").b64,
    cheerio = require('cheerio'),
    rootUrl = "https://www.google.com",
    querySelector = '#title-overview-widget > div.vital > div.title_block > div > div.titleBar > div.title_wrapper > h1',
    imdb_root_url = "http://www.imdb.com/";
    g = module.exports = {},

    buildSearchQ = function(opts){
        var query = rootUrl + "/search?q="+encodeURI(opts["title"].toLowerCase())+"+movie";
        return new Promise(function(resolve,reject){
              opts["searchQ"] = query;
              logger.debug("( "+opts["title"]+" ) "+  "Search Query built : " + opts["searchQ"]);
              resolve(opts);
        })
    },

    loadSearchResults = function(opts){
        return loader.loadDoc("searchQ",opts);
    },

    loadImdbDoc = function(opts){
        return loader.loadDoc("imdbDocUrl",opts);
    },

    findTitle = function(opts){
      return new Promise(function(resolve,reject){
          var body = opts["body"];
          $ = cheerio.load(body);
          var title = $(querySelector).text().trim();

            opts["title"] = title;
            logger.debug("( "+opts["title"]+" ) "+"Official title found : " + opts["title"]);
            resolve(opts);
      })
    },

    findImdbDocUrl = function(opts){
      $ = cheerio.load(opts["body"]);
      var querySelector = ".g > h3 > a";
      var card = $(querySelector);
      opts["imdbDocUrl"] = "N/A";

      for(i = 0; i < card.length; i++){
        var text = $(card[i]).text();
        var link = $(card[i]).attr("href").replace('/url?q=','').split('&')[0];
        if(stringUtils(link).contains("imdb.com")){
            opts["imdbDocUrl"] = link;
            break;
        }
      }

      return new Promise(function(resolve,reject){
            logger.debug("( "+opts["title"]+" ) "+  "ImdbDocUrl found: " + opts["imdbDocUrl"]);
            resolve(opts);
      })

    }
;


g.fetchTitle = function(title){
    return new Promise(function(ft_resolve,ft_reject){
      var opts = {};
      opts["title"] = title;
      logger.debug("-->")
      logger.debug("( "+opts["title"]+" ) "+  "Entering G title fetcher");
      buildSearchQ(opts)
      .then(loadSearchResults)
      .then(findImdbDocUrl)
      .then(function(opts){
          if(opts["imdbDocUrl"]==="N/A"){
            return new Promise(function(resolve,reject){
                if(opts['title'].trim()){
                    ft_resolve(opts['title']);
                }else{
                    logger.debug("( "+opts["title"]+" ) "+  "No official title found. returning : " + title);
                    ft_resolve(title);
                }
            });
          }
          loadImdbDoc(opts)
          .then(findTitle)
          .then(function(opts){
                  if(opts['title'].trim()){
                      ft_resolve(opts['title']);
                  }else{
                      logger.debug("( "+opts["title"]+" ) "+  "No official title found. returning : " + title);
                      ft_resolve(title);
                  }
          })
      });
    });
}
