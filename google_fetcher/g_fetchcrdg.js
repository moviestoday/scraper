var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    stringUtils = require("../utils/utils.js").stringUtils,
    b64 = require("../utils/utils.js").b64,
    cheerio = require('cheerio'),
    rootUrl = "https://www.google.com",
    imdb_root_url = "http://www.imdb.com/";
    g = module.exports = {},

    buildSearchQ = function(opts){
        var query = rootUrl + "/search?q="+encodeURI(opts["title"].toLowerCase())+"+movie";
        return new Promise(function(resolve,reject){
              opts["searchQ"] = query;
              logger.debug("( "+opts["title"]+" ) "+  "Search Query built : " + opts["searchQ"]);
              resolve(opts);
        })
    },

    loadSearchResults = function(opts){
        return loader.loadDoc("searchQ",opts);
    },

    loadImdbDoc = function(opts){
        return loader.loadDoc("imdbDocUrl",opts);
    },

    findCRDG = function(opts){
      /*
      CR -  Content Rating
      D -   Duration
      G -   Genre
      */
      var body = opts["body"];
      $ = cheerio.load(body);
      var cr_qs = "#overview-top > div.infobar > meta",
          d_qs = "#overview-top > div.infobar > time",
          g_qs = "span[itemprop=genre]",
          cr = $(cr_qs).attr('content')|| "N/A",
          d = $(d_qs).text() || "N/A",
          _g = $(g_qs),
          g = "";
          cr = cr.trim();
          d = d.trim();


      for(i =0; i < _g.length; i++)
      {
        if(i > 0){
          g+= ", ";
        }
        g += $(_g[i]).text();
      }

      return new Promise(function(resolve,reject){
            opts["content_rating"] = cr;
            opts["duration"] = d;
            opts["genre"] = g;
            logger.debug("( "+opts["title"]+" ) "+"Fetched CRDG : " + opts["content_rating"] + ", " + opts["duration"] + ", " + opts["genre"]);
            resolve(opts);
      })
    },

    findImdbDocUrl = function(opts){
      $ = cheerio.load(opts["body"]);
      var querySelector = ".g > h3 > a";
      var card = $(querySelector);
      opts["imdbDocUrl"] = "N/A";

      for(i = 0; i < card.length; i++){
        var text = $(card[i]).text();
        var link = $(card[i]).attr("href").replace('/url?q=','').split('&')[0];
        if(stringUtils(link).contains("imdb.com")){
            opts["imdbDocUrl"] = link;
            break;
        }
      }

      return new Promise(function(resolve,reject){
            logger.debug("( "+opts["title"]+" ) "+  "ImdbDocUrl found: " + opts["imdbDocUrl"]);
            resolve(opts);
      })

    }
;


g.fetchCRDG = function(title){
    return new Promise(function(ft_resolve,ft_reject){
      var opts = {};
      opts["title"] = title;
      logger.debug("-->")
      logger.debug("( "+opts["title"]+" ) "+  "Entering G poster fetcher");
      buildSearchQ(opts)
      .then(loadSearchResults)
      .then(findImdbDocUrl)
      .then(function(opts){
          if(opts["imdbDocUrl"]==="N/A"){
            return new Promise(function(resolve,reject){
                  ft_resolve(opts);
            });
          }
          loadImdbDoc(opts)
          .then(findCRDG)
          .then(function(opts){
              ft_resolve(opts);
          })
      });
    });
}
