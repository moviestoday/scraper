var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    stringUtils = require("../utils/utils.js").stringUtils,
    b64 = require("../utils/utils.js").b64,
    cheerio = require('cheerio'),
    rootUrl = "https://www.google.com",
    imdb_root_url = "http://www.imdb.com/",
    poster_404_url = "http://teradactol.github.io/moviestoday/poster_404.jpg";
    g = module.exports = {},

    build_Poster_SearchQ = function(opts){
        var query = rootUrl + "/search?q="+encodeURI(opts["title"].toLowerCase())+"+movie";
        return new Promise(function(resolve,reject){
              opts["searchQ"] = query;
              logger.debug("( "+opts["title"]+" ) "+  "Search Query built : " + opts["searchQ"]);
              resolve(opts);
        })
    },

    loadSearchResults = function(opts){
        return loader.loadDoc("searchQ",opts);
    },

    loadImdbDoc = function(opts){
        return loader.loadDoc("imdbDocUrl",opts);
    },

    loadWikiDoc = function(opts){
        return loader.loadDoc("WikiDocUrl",opts);
    },

    loadPosterDoc = function(opts){
      return loader.loadDoc("posterDocUrl",opts)
    },

    loadPosterDoc = function(opts){
      return loader.loadDoc("posterDocUrl",opts)
    },

    findPosterImgUrl = function(opts){
      var body = opts["body"];
      $ = cheerio.load(body);
      var querySelector = "#primary-img"
      var url = $(querySelector).attr('src');
      return new Promise(function(resolve,reject){
            opts["posterImgUrl"] = url;
            logger.debug("( "+opts["title"]+" ) "+"PosterImg Url found : " + opts["posterImgUrl"]);
            resolve(opts);
      })
    },

    findPosterDocUrl = function(opts){
      var body = opts["body"];
      $ = cheerio.load(body);
      var querySelector = "div.poster > a"
      var url = $(querySelector).attr('href');
      return new Promise(function(resolve,reject){
            opts["posterDocUrl"] = imdb_root_url+ url
            if(typeof(url)==="undefined"){
                opts["posterDocUrl"] = "N/A";
            }
            logger.debug("( "+opts["title"]+" ) "+"PosterDoc Url found : " + opts["posterDocUrl"]);
            resolve(opts);
      })
    },

    findPosterDocUrl_Wiki = function(opts){
      var body = opts["body"];
      $ = cheerio.load(body);
      var querySelector = ".image img"
      var url = $(querySelector).attr('src');
      console.log("Text : " + url)
      return new Promise(function(resolve,reject){
            opts["posterImgUrl"] = "https:"+ url
            if(typeof(url)==="undefined"){
                opts["posterImgUrl"] = "N/A";
            }
            logger.debug("( "+opts["title"]+" ) "+"PosterImg Url found(Wiki) : " + opts["posterImgUrl"]);
            resolve(opts);
      })
    },

    find_Poster_ImdbDocUrl = function(opts){
      $ = cheerio.load(opts["body"]);
      var querySelector = ".g";
      var card = $(querySelector);
      opts["imdbDocUrl"] = "N/A";

      for(i = 0; i < card.length; i++){
        var url = $(card[i]).find('cite').text();
        if(stringUtils(url).contains("imdb.com")){
            opts["imdbDocUrl"] = "http://"+url;
            opts["imdbDocUrl"] = opts["imdbDocUrl"].replace('fullcredits/','');
        }
        if(stringUtils(url).contains("wikipedia.org")){
            opts["WikiDocUrl"] = url;
        }
      }

      return new Promise(function(resolve,reject){
            logger.debug("( "+opts["title"]+" ) "+  "WikiDocUrl found: " + opts["WikiDocUrl"]);
            logger.debug("( "+opts["title"]+" ) "+  "ImdbDocUrl found: " + opts["imdbDocUrl"]);
            resolve(opts);
      })

    }
;


g.fetchPoster = function(title){
    return new Promise(function(fp_resolve,fp_reject){
      var opts = {};
      opts["title"] = title;
      logger.debug("-->")
      logger.debug("( "+opts["title"]+" ) "+  "Entering G poster fetcher");
      build_Poster_SearchQ(opts)
      .then(loadSearchResults)
      .then(find_Poster_ImdbDocUrl)
      .then(function(opts){
          if(opts["imdbDocUrl"]==="N/A"){
            var url = poster_404_url;
            opts["posterImgUrl"] = url;
            logger.debug("( "+opts["title"]+" ) "+  "PosterDocUrl not found returning posterImg url: " + url);
            b64.toBase64("posterImgUrl",opts)
            .then(function(opts){
                    fp_resolve(opts["posterImgUrl"]);
            })
            return;
          }
          loadImdbDoc(opts)
          .then(findPosterDocUrl)
          .then(function(opts){
              opts["posterDocUrl"] = opts["posterDocUrl"] || "N/A";
              if(opts["posterDocUrl"].trim()==="N/A"){
                  opts["WikiDocUrl"] = opts["WikiDocUrl"] || "N/A";
                  if(opts["posterDocUrl"].trim()==="N/A"){
                      var url = poster_404_url;
                      opts["posterImgUrl"] = url;
                      logger.debug("( "+opts["title"]+" ) "+  "PosterDocUrl not found returning posterImg url: " + url);
                      b64.toBase64("posterImgUrl",opts)
                          .then(function(opts){
                              fp_resolve(opts["posterImgUrl"]);
                          })
                      return;
                  }else{
                    loadWikiDoc(opts)
                    .then(findPosterDocUrl_Wiki)
                    .then(function(opts){
                        if(opts["posterImgUrl"]==="N/A"){
                          var url = poster_404_url;
                          opts["posterImgUrl"] = url;
                          logger.debug("( "+opts["title"]+" ) "+  "PosterDocUrl not found returning posterImg url: " + url);
                          b64.toBase64("posterImgUrl",opts)
                          .then(function(opts){
                                  fp_resolve(opts["posterImgUrl"]);
                          })
                          return;
                        }else{
                          b64.toBase64("posterImgUrl",opts)
                          .then(function(opts){
                                  fp_resolve(opts["posterImgUrl"]);
                                  return;
                          })
                        }

                    })
                  }
              }else{
                loadPosterDoc(opts)
                .then(findPosterImgUrl)
                .then(function(opts){
                    b64.toBase64("posterImgUrl",opts)
                    .then(function(opts){
                            fp_resolve(opts["posterImgUrl"]);
                    })
                })
              }

          })

      });

    });
}
