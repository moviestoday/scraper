var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    stringUtils = require("../utils/utils.js").stringUtils,
    b64 = require("../utils/utils.js").b64,
    cheerio = require('cheerio'),
    rootUrl = "https://www.google.com",
    imdb_root_url = "http://www.imdb.com/";
    g = module.exports = {},

    build_trailer_SearchQ = function(opts){
        var query = rootUrl + "/search?q="+encodeURI(opts["title"].toLowerCase())+"+trailer";
        return new Promise(function(resolve,reject){
              opts["searchQ"] = query;
              logger.debug("( "+opts["title"]+" ) "+  "Search Query built : " + opts["searchQ"]);
              resolve(opts);
        })
    },

    loadSearchResults = function(opts){
        return loader.loadDoc("searchQ",opts);
    },

    findYouTubeUrl = function(opts){
      return new Promise(function(resolve,reject){
          $ = cheerio.load(opts["body"]);
          var querySelector = ".g";
          var card = $(querySelector);
          opts["trailer"] = "N/A";
          for(i = 0; i < card.length; i++){
            var title = $(card[i]).find('.r').text();
            title = title.toLowerCase();
            var url = $(card[i]).find('cite').text();
            if(stringUtils(title).contains("official")){
                opts["trailer"] = url;
                break;
            }
          }
          logger.debug("( "+opts["title"]+" ) "+  "YouTubeUrl found: " + opts["trailer"]);
          resolve(opts);
      })
    };

    g.fetchTrailer = function(title){
        return new Promise(function(ft_resolve,ft_reject){
          var opts = {};
          opts["title"] = title;
          logger.debug("-->")
          logger.debug("( "+opts["title"]+" ) "+  "Entering G trailer fetcher");
          build_trailer_SearchQ(opts)
          .then(loadSearchResults)
          .then(findYouTubeUrl)
          .then(function(opts){
              ft_resolve(opts);
          })
        });
    };
