var g_fp = require("./g_fetchposter"),
    g_ft = require("./g_fetchtitle"),
    g_fcrdg = require("./g_fetchcrdg"),
    g_ftr = require("./g_fetchtrailer"),
    g = module.exports = {};

g.fetchPoster = g_fp.fetchPoster;
g.fetchTitle = g_ft.fetchTitle;
g.fetchCRDG = g_fcrdg.fetchCRDG;
g.fetchTrailer = g_ftr.fetchTrailer;
