var Promise = require("../utils/utils").promise,
    workPool = require("../utils/utils").async,
    g = require("../google_fetcher/g"),
    scrape = module.exports = {};

scrape.scrape = function(opts){
    return new Promise(function(sc_resolve,sc_reject){
        var n_movies = [], movies = opts["movies"];
        workPool.each(movies,function(movie,wp_1_callback){
            var sc_Async = [];

            sc_Async.push(function(callback){
                g.fetchTitle(movie["title"]).then(function(title){
                    movie["title"]=title;
                    callback();
                })
            });

            sc_Async.push(function(callback){
                g.fetchPoster(movie["title"]).then(function(poster){
                    movie["poster"]=poster;
                    callback();
                })
            });

            sc_Async.push(function(callback){
                g.fetchCRDG(movie["title"]).then(function(opts){
                    movie["content_rating"]=opts["content_rating"] || "N/A";
                    movie["duration"]=opts["duration"] || "N/A";
                    movie["genre"]=opts["genre"] || "N/A";
                    callback();
                })
            });

            sc_Async.push(function(callback){
                g.fetchTrailer(movie["title"]).then(function(opts){
                    movie["trailer"]=opts["trailer"] || "N/A";
                    callback();
                })
            });


            workPool.parallel(sc_Async,function(err,results){
                n_movies.push(movie);
                wp_1_callback();
            });

        },function(err){
            opts["n_movies"] = n_movies;
            sc_resolve(opts);
        })

    });
}
