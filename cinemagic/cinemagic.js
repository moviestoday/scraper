var csoon = require("./cinemagic_getComingSoon"),
    nshow = require("./cinemagic_getNowShowing"),
    cinemagic = module.exports = {};

cinemagic.getNowShowing = nshow.getNowShowing;
cinemagic.getComingSoon = csoon.getComingSoon;
