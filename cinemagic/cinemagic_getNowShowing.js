var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    workPool = require("../utils/utils.js").async,
    cheerio = require('cheerio'),
    core = require("../core/core"),
    cinemagic = module.exports = {},
    rootUrl = "http://www.metroplexmall.com/",

    loadMainPage = function(opts){
      return loader.loadDoc("rootUrl",opts);
    },

    loadCinemaPage = function(opts){
      return loader.loadDoc("cinemaDocUrl",opts);
    },

    getmovie = function(gm_url){
      return new Promise(function(gm_resolve, gm_reject){
          logger.debug("GetMovie Url got : " + gm_url);
          var opts = {};
          var querySelector = ".official6 > b:nth-child(1)",
              st_qs = ".official6", _3d_qs = ".official5";

          opts["movie"] = gm_url;
          loader.loadDoc("movie",opts)
          .then(function(opts){
                var _$ = cheerio.load(opts["body"]);
                  var movie = _$(querySelector).text(),
                      showtime_str = _$(st_qs).clone().children().remove().end().text().trim().split('\n'),
                      _3d = (_$(_3d_qs).text()+"").toLowerCase().search("3d")>-1,
                      showtimes=[];
                      _3d = _3d+"";

                  for(var i = showtime_str.length-1; i > -1; i--){
                      var txt = showtime_str[i].trim().toLowerCase();
                      if(txt.search('showtimes')>-1){
                          txt = txt.replace('showtimes:','').trim().split(',');
                          txt.forEach(function(showtime){
                              showtimes.push(showtime.trim());
                          });
                          break;
                      }else if( txt.search('showtime')>-1 ){
                          txt = txt.replace('showtime:','').trim();
                          if(txt.search(',')>-1){
                              txt = txt.split(',');
                              txt.forEach(function(showtime){
                                  showtimes.push(showtime.trim());
                              });
                          }else{
                              showtimes.push(txt.trim())
                          }
                      }
                  }

                  opts["movie"]=movie;
                  opts["showtimes"]=showtimes;
                  opts["_3D"]=_3d;
                  gm_resolve(opts);
          })
      })
    },

    findNowShowingMovieTitles = function(fmt_opts){
        //for each
        //get movie
        return new Promise(function(fmt_resolve, fmt_reject){
            var movies = [];
            workPool.each(fmt_opts["nsMovieUrls"],function(url,callback){
                getmovie(url)
                .then(function(opts){
                        var movie = opts["movie"];
                        var showtimes = opts["showtimes"];
                        movies.push({"title":movie,"showtimes":showtimes,"cinema":"cinemagic","_3D":opts["_3D"],"poster":"http://teradactol.github.io/moviestoday/poster_404.png"});
                        callback();
                })
            },function(err){
                logger.debug("Movies : " + movies);
                var opts = {};
                opts["movies"]=movies;
                fmt_resolve(opts);
            });

        });
    },

    findNowShowingMovieUrls = function(opts){
        return new Promise(function(resolve,reject){
              var body = opts["body"];
              $ = cheerio.load(body);
              var querySelector = "#country1 > table > tr > td > span";
              var rawMovies = $(querySelector);
              var nsMovieUrls = [];
              for(i = 0; i < rawMovies.length; i++){
                var url = $(rawMovies[i]).find('a').attr('href');
                url = rootUrl+encodeURI(url);
                nsMovieUrls.push(url);
              }

              opts["nsMovieUrls"] = nsMovieUrls
              logger.debug("nsMovieUrls found : " + opts["nsMovieUrls"]);
              resolve(opts);
        })
    },

    findCinemaDocUrl = function(opts){
      var body = opts["body"];
      $ = cheerio.load(body);
      var querySelector = "body > table > tr:nth-child(14) > td > a:nth-child(11)"
      var url = $(querySelector).attr('href');
      url = encodeURI(url);
      return new Promise(function(resolve,reject){
            opts["cinemaDocUrl"] = rootUrl + url
            if(typeof(url)==="undefined"){
                opts["cinemaDocUrl"] = root_url;
            }
            logger.debug("CinemaDoc Url found : " + opts["cinemaDocUrl"]);
            resolve(opts);
      })
    }

;




cinemagic.getNowShowing = function(){
  //Got to cinemagic main page
  //Get cinema link
  //Go to cinema page
  //Get all now showing links
  //Go to all links fetch the now showing titles
  //scrape the niggas !!
    return new Promise(function(gns_resolve, gns_reject){
        var opts = {};
        opts["rootUrl"] = rootUrl;
        loadMainPage(opts)
            .then(findCinemaDocUrl)
            .then(loadCinemaPage)
            .then(findNowShowingMovieUrls)
            .then(findNowShowingMovieTitles)
            .then(core.scrape)
            .then(function(opts){
                gns_resolve(opts['n_movies']);
            })
        ;
    });
};
