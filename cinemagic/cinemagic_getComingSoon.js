var logger = require("../utils/utils.js").logger,
    loader = require("../utils/utils.js").loader,
    Promise = require("../utils/utils.js").promise,
    cheerio = require('cheerio'),
    cinemagic = module.exports = {},
    rootUrl = "http://www.metroplexmall.com/";



cinemagic.getComingSoon = function(){
  //Got to cinemagic main page
  //Get cinema link
  //Go to cinema page
  //Get all now showing links
  //Go to all links fetch the now showing titles
  //scrape the titles
  var opts = {};
  opts["rootUrl"] = rootUrl;
  loadMainPage(opts)
  .then(findCinemaDocUrl)
  .then(loadCinemaPage)
  .then(findNowShowingMovieUrls)
  .then(findNowShowingMovieTitles)
  ;

}
