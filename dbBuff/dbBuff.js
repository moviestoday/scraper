var   Promise = require('../utils/utils').promise,
      logger = require('../utils/utils').logger,
      dbStore = require('../utils/utils').db,
      cinemax = require('../cinemax/cinemax'),
      cinemagic = require('../cinemagic/cinemagic'),
      cinemas = {},
      csoondb = process.env.OPENSHIFT_DATA_DIR + "/moviestoday_db/csoon.json",
      nshowdb = process.env.OPENSHIFT_DATA_DIR + "/moviestoday_db/nshow.json",
      db = {
        csoon:new dbStore({filename:csoondb,autoload:true}),
        nshow:new dbStore({filename:nshowdb,autoload:true})
      }
      dbBuff = module.exports = {};

cinemas["cinemax"] = cinemax;
cinemas["cinemagic"] = cinemagic;

dbBuff.getComingSoon = function(cinema){
  return new Promise(function(gcs_resolve,gcs_reject){
      logger.debug("Cinema recieved : " + cinema);
      //loadComing soon movies from db
      db.csoon.find({cinema:cinema},function(err,docs){
          gcs_resolve(docs);

          cinemas[cinema].getComingSoon()
          .then(function(movies){
              //delete stale movies in db
              db.csoon.remove({ cinema:cinema},{ multi: true }, function (err, numRemoved){
                  //Put fresh movies
                  db.csoon.insert(movies, function (err, newDocs) {
                      logger.debug("Finished updating csoon db");
                  });
              });
          });

      });



  })
}

dbBuff.getNowShowing = function(cinema){
  return new Promise(function(gcs_resolve,gcs_reject){
      logger.debug("Cinema recieved : " + cinema);
      //loadComing soon movies from db
      db.nshow.find({cinema:cinema},function(err,docs){
          gcs_resolve(docs);
          
          cinemas[cinema].getNowShowing()
          .then(function(movies){
              //delete stale movies in db
              db.nshow.remove({ cinema:cinema},{ multi: true }, function (err, numRemoved){
                  //Put fresh movies
                  db.nshow.insert(movies, function (err, newDocs) {
                      logger.debug("Finished updating nshow db");
                  });
              });
          });

      });



  })
}
