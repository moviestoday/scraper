var Promise = require('bluebird'),
    request = require("request"),
    loader = module.exports = {};

loader.loadDoc = function(what,opts){
  var url = opts[what];
  return new Promise(function(resolve,reject){
    request(url,function(err,response,body){
        opts["body"] = body;
        resolve(opts);
    });
  })
}
