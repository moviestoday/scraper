var
    http = require('http'),
    express = require('express'),
    cinemas = require('./dbBuff/dbBuff'),
    compress = require('compression'),
    async = require('async'),
    app = express()
;

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1');
app.use(compress());
app.use(function(req, res, next) {

  var responseSettings = {
      "AccessControlAllowOrigin": "*",
      "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
      "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
      "AccessControlAllowCredentials": true
  };

  res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
  res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
  res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
  res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

  next();
});

http.createServer(app).listen(app.get('port'), app.get('ip'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

app.get('/', function (req, res) {
    res.send('Hello World! from wercker');
});

app.get('/wscinemax/comingsoon', function (req, res) {
  cinemas.getComingSoon("cinemax")
  .then(function(movies){
    res.json(movies)
  });
});

app.get('/api/v2/wscinemax/comingsoon', function (req, res) {
    cinemas.getComingSoon("cinemax")
        .then(function(movies){
            var movies_arr = [];
            movies.forEach(function(movie){
                movie.poster="";
                movies_arr.push(movie);
            });
            res.json(movies_arr)
        });
});


app.get('/wscinemax/nowshowing', function (req, res) {
  cinemas.getNowShowing("cinemax")
  .then(function(movies){
    res.json(movies)
  });
});

app.get('/api/v2/wscinemax/nowshowing', function (req, res) {
    cinemas.getNowShowing("cinemax")
        .then(function(movies){
            var movies_arr = [];
            movies.forEach(function(movie){
                movie.poster="";
                movies_arr.push(movie);
            });
            res.json(movies_arr)
        });
});


app.get('/wscinemagic/nowshowing', function (req, res) {
    cinemas.getNowShowing("cinemagic")
        .then(function(movies){
            res.json(movies)
        });
});

app.get('/api/v2/wscinemagic/nowshowing', function (req, res) {
    cinemas.getNowShowing("cinemagic")
        .then(function(movies){
            var movies_arr = [];
            movies.forEach(function(movie){
                movie.poster="";
                if(movie.title)
                    movies_arr.push(movie);
            });
            res.json(movies_arr)
        });
});

app.get('/api/v2/poster/:title', function (req, res) {
    var movie_title = req.params.title;
    require('./google_fetcher/g_fetchposter').fetchPoster(movie_title).then(function(uri){
        var result = {"uri":uri};
        res.json(result)
    });
});

app.get('/all', function (req, res) {
      async.parallel([
        function(done){
          cinemas.getComingSoon("cinemax")
          .then(function(movies){
            done(null,movies)
          });
        },
        function(done){
          cinemas.getNowShowing("cinemax")
          .then(function(movies){
            done(null,movies)
          });
        },
        function(done){
          cinemas.getNowShowing("cinemagic")
          .then(function(movies){
              done(null,movies)
          });
        },
      ],function(err,results){
          var movies = {cinemax:{},cinemagic:{}};
          movies["cinemax"]["csoon"]=results[0];
          movies["cinemax"]["nshow"]=results[1];
          movies["cinemagic"]["nshow"]=results[2];
          res.json(movies)
      })
});

app.get('/api/v2/all', function (req, res) {
    async.parallel([
        function(done){
            cinemas.getComingSoon("cinemax")
                .then(function(movies){
                    var movies_arr = [];
                    movies.forEach(function(movie){
                        movie.poster="";
                        if(movie.title)
                            movies_arr.push(movie);
                    });
                    done(null,movies_arr);
                });
        },
        function(done){
            cinemas.getNowShowing("cinemax")
                .then(function(movies){
                    var movies_arr = [];
                    movies.forEach(function(movie){
                        movie.poster="";
                        if(movie.title)
                            movies_arr.push(movie);
                    });
                    done(null,movies_arr);
                });
        },
        function(done){
            cinemas.getNowShowing("cinemagic")
                .then(function(movies){
                    var movies_arr = [];
                    movies.forEach(function(movie){
                        movie.poster="";
                        if(movie.title)
                            movies_arr.push(movie);
                    });
                    done(null,movies_arr);
                });
        },
    ],function(err,results){
        var movies = [],
            cinemax = {},
            cinemagic = {};

        cinemax["csoon"]=results[0];
        cinemax["nshow"]=results[1];
        cinemax["title"]="Century Cinemax (Acacia Mall)";
        cinemagic["nshow"]=results[2];
        cinemagic["title"]="Cinema Magic (Metroplex Mall)";
        movies.push(cinemax);
        movies.push(cinemagic);
        res.json(movies)
    })
});